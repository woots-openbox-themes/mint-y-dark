===================
Openbox Mint-Y Dark
===================

An Openbox theme matching the Mint-Y-Dark GTK theme.

* GTK theme: Mint-Y-Dark
* Icons: Mint-X
* Font: Cantarell
* Programs: Conky system monitor, PCManFM, Sakura terminal

.. image:: doc-screenshot.png
   :width: 892
   :target: screenshot.png
